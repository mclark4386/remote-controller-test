Setup
-----

`pip3 install pygame pyzmq`

Run
-----

On Receiver:
`python3 controller_client.py`

On Controller Server:
`python3 controller_server.py`