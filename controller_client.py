import time
import zmq

class GamePadReceiver():
    def __init__(self,zmqstring):
        self.recv = zmq.Context.instance().socket(zmq.PULL)
        self.recv.bind(zmqstring)
        self.poller = zmq.Poller()
        self.poller.register(self.recv, zmq.POLLIN)

    def run(self):
        while True:
            socks = dict(self.poller.poll())

            if socks.get(self.recv) == zmq.POLLIN:
                signal = self.recv.recv_string()
                self.signal = signal
                print(self.signal)


if __name__ == '__main__':
    host = "0.0.0.0"
    port = "987876"
    gp = GamePadReceiver("tcp://"+host+":"+port)
    gp.run()
