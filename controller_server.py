import time
import pygame
import zmq

class GamePad():

    def __init__(self,zmqstring):
        pygame.init()
        pygame.joystick.init()
        self.joys = [pygame.joystick.Joystick(x) for x in range(pygame.joystick.get_count())]
        [j.init() for j in self.joys]
        self.sender = zmq.Context.instance().socket(zmq.PUSH)
        self.sender.connect(zmqstring)
        self.ret = len(self.joys)*[None]
        for i,j in enumerate(self.joys):
            self.ret[i] = (j.get_numbuttons()+j.get_numaxes()+j.get_numhats()) * [0]

    def get_all(self):
        pygame.event.pump()

        for i,j in enumerate(self.joys):
            num_buttons = j.get_numbuttons()
            for x in range(num_buttons):
                self.ret[i][x] = j.get_button(x)

            num_axes = j.get_numaxes()
            for x in range(num_axes):
                self.ret[i][x+num_buttons] = j.get_axis(x)

            for x in range(j.get_numhats()):
                self.ret[i][x+num_buttons+num_axes] = j.get_hat(x)

        return self.ret

    def test(self):
        while True:
            print (self.get_all())

    def serve_all(self,delay):
        while True:
            self.sender.send_string(str(self.get_all()))
            time.sleep(delay)


if __name__ == '__main__':
    host = "localhost"
    port = "987876"
    gp = GamePad("tcp://"+host+":"+port)
    delay = 250/1000.0
    gp.serve_all(delay)
